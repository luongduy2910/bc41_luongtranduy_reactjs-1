import React, { Component } from "react";
import Header from "./Header";
import Banner from "./Banner";
import Content from "./Content";
import Footer from "./Footer";

class Layout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default Layout;
