import './App.css';
import Layout from './BaiTap_Layout/Layout';

function App() {
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;
